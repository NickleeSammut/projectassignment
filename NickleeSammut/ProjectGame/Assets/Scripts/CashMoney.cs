﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CashMoney : MonoBehaviour {

    public float dollas;
    Text coinText;
    // Use this for initialization
    void Start()
    {
        coinText = GameObject.Find("CoinAmount").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        coinText.text = " " + dollas;
    }

    public void CoinCollected()
    {
        dollas += 1;
    }

    void win()
    {
        if(coinText.text == "10")
        {
            SceneManager.LoadScene("Level 2");
        }
    }
}
