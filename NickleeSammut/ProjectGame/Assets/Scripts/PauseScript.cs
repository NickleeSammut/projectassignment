﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour {

    GameObject[] pause;
    // Use this for initialization
    void Start () {
        pause = GameObject.FindGameObjectsWithTag("pause");
        foreach (GameObject pauobj in pause)
        {
            pauobj.SetActive(false);
        }
    }

    public void Menu()
    {
        SceneManager.LoadScene("Start");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PauseGame()
    {
        Time.timeScale = 0;
        foreach (GameObject pauobj in pause)
        {
            pauobj.SetActive(true);
        }
    }

    public void unPause()
    {
        Time.timeScale = 1;
        foreach (GameObject pauobj in pause)
        {
            pauobj.SetActive(false);
        }
    }
}
