﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    //Speed of Character
    public float speed;

    //How high the player jumps
    public float jumpForce;

    //Detects if left or right key is pressed and moves correspondingly
    private float moveInput;

    private Rigidbody2D rb;

    private bool facingRight = true;

    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatisGround;

    private int extraJump;
    public int extraJumpValue;

    public Animator myAnimator;

    CashMoney cm;

    public float health;
    public float maxhealth;
    Image healthbar;

    public GameObject CoinParticles;

    void Start()
    {
        healthbar = GameObject.Find("HealthBar").GetComponent<Image>();
        maxhealth = health;
        myAnimator = GetComponent<Animator>();
        extraJump = extraJumpValue;
        rb = GetComponent<Rigidbody2D>();
        cm = GameObject.Find("CashMoney").GetComponent<CashMoney>();
    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatisGround);
        moveInput = Input.GetAxis("Horizontal");
        Debug.Log(moveInput);
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        if(facingRight == false && moveInput > 0)
        {
            Flip();
        } else if(facingRight == true && moveInput < 0)
        {
            Flip();
        }
    }

    void Update()
    {

        healthbar.fillAmount = health / maxhealth;

        float mySpeed = Mathf.Abs(moveInput);

        myAnimator.SetFloat("Speed", mySpeed);

        if(rb.velocity.y == 0)
        {
            myAnimator.SetBool("isJumping", false);
        }

        if (rb.velocity.y > 0)
        {
            myAnimator.SetBool("isJumping", true);
        }

        if (isGrounded == true)
        {
            extraJump = extraJumpValue;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && extraJump > 0)
        {
            rb.velocity = Vector2.up * jumpForce;
            extraJump--;

        }else if(Input.GetKeyDown(KeyCode.UpArrow) && extraJump == 0 && isGrounded ==true)
        {
            rb.velocity = Vector2.up * jumpForce;

        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Coin")
        {
            cm.CoinCollected();
            Instantiate(CoinParticles, other.transform.position, Quaternion.identity);
            Destroy(other.gameObject);
        }
    }

    public void GetDamage(float dmg)
    {
        myAnimator.SetTrigger("Hurt");
        health -= dmg;
        if(health <= 0)
        {
            SceneManager.LoadScene("Start");
        }
    }
}
